let matchesOfIpl = require('../data/matches.json');
let deliveriesOfIpl = require('../data/deliveries.json'); 


// let  deliveriesOfIpl= require('./deliveries.json');
// let matchesOfIpl=require('./matches.json')
const fs= require('fs');
let ipl =require('./ipl.js')


let numberOfMatchesPerYear=ipl.matchesPerYear(matchesOfIpl);
//  console.log(numberOfMatchesPerYear)
 fs.writeFile('../public/output/matchesPerYear.json', JSON.stringify(numberOfMatchesPerYear), (err) => { 
            if (err) console.log(err)
        })

let matchesWonPerTeamResult=ipl.matchesWonPerTeam(matchesOfIpl);
// console.log(matchesWonPerTeamResult)
fs.writeFile('../public/output/matchesWonPerTeamResult.json', JSON.stringify(matchesWonPerTeamResult), (err) => { 
      if (err) console.log(err)
  })

let extraRunsConceded2016Result=ipl.extraRunsConceded2016(matchesOfIpl,deliveriesOfIpl)
// console.log(extraRunsConceded2016Result)
fs.writeFile('../public/output/extraRunsConcided2016Result.json', JSON.stringify(extraRunsConceded2016Result), (err) => { 
     if (err) console.log(err)
})


let mostEcoResult=ipl.mostEconomicalBowler2015(matchesOfIpl,deliveriesOfIpl)
// console.log(mostEcoResult)
fs.writeFile('../public/output/mostEcoResult.json', JSON.stringify(mostEcoResult), (err) => { 
      if (err) console.log(err)
  })

let totalKohliRunsOf2016Result=ipl.kohliRuns2016(matchesOfIpl,deliveriesOfIpl)
// console.log(totalKohliRunsOf2016Result)
fs.writeFile('../public/output/totalKohliRunsOf2016Result.json', JSON.stringify(totalKohliRunsOf2016Result), (err) => { 
      if (err) console.log(err)
})


let bowledByBumrahResult=ipl.bumrahBowled2016(matchesOfIpl,deliveriesOfIpl)
// console.log(bowledByBumrahResult)
fs.writeFile('../public/output/bowledByBumrahResult.json', JSON.stringify(bowledByBumrahResult), (err) => { 
      if (err) console.log(err)
})
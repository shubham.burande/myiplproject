let matchesOfIpl = require('../data/matches.json');
let deliveriesOfIpl = require('../data/deliveries.json'); 
const fs= require('fs');

////////////////////////////////function 1//////////////////////////////////////

 function matchesPerYear(matchesJson){
      let iplYears=matchesJson.reduce((updatedMatches,currentMatch)=> {
                  if(updatedMatches.hasOwnProperty(currentMatch.season))
                  updatedMatches[currentMatch.season] += 1;
                  else updatedMatches[currentMatch.season] = 1;
                  return updatedMatches;
            },{})
     return iplYears
}
let matchesPerYearResult=matchesPerYear(matchesOfIpl);
//console.log(matchesPerYearResult);

////////////////////////////////function 2///////////////////////////////////////

function matchesWonPerTeam(matches){
      let matchesWonPerYearPerTeam=matches.reduce((matchObj,currentMatch)=>{
            if(matchObj.hasOwnProperty(currentMatch.season)){  // checks if thats season in it
                  if(matchObj[currentMatch.season].hasOwnProperty(currentMatch.winner)){    
                        matchObj[currentMatch.season][currentMatch.winner]+=1;
                  }
                  else{
                        if(currentMatch.winner!='')           //checks if the match was tied
                        matchObj[currentMatch.season][currentMatch.winner]=1
                  }
            }
            else matchObj[currentMatch.season]={};             // adds that season in matchObj 
            return matchObj;
      },{})
       return matchesWonPerYearPerTeam;
}
      
let matchesPerTeamResult=matchesWonPerTeam(matchesOfIpl);
//console.log(matchesPerTeamResult);


///////////////////////////////function 3///////////////////////////////////////

function extraRunsConceded2016(matchesJson, deliveryJson) {
      // creates an array of id os matches in 2016
      let idOf2016 = matchesJson.filter((match)=> match.season == 2016).map((match)=> parseInt(match.id));
     
      //create an object which has extra runs conceded by each team 
      let extraRuns=deliveryJson.reduce((extraRunsObj,currentBall)=>{
            if(idOf2016.includes(parseInt(currentBall.match_id))){
                  if(extraRunsObj.hasOwnProperty(currentBall.bowling_team))
                        extraRunsObj[currentBall.bowling_team]+=parseInt(currentBall.extra_runs)
                  else
                        extraRunsObj[currentBall.bowling_team]=parseInt(currentBall.extra_runs)
            }
            return extraRunsObj;
      },{})
      return extraRuns
}
      
let extraRunsResult=extraRunsConceded2016(matchesOfIpl,deliveriesOfIpl)
//console.log(extraRunsResult);

// console.log(deliveriesOfIpl = require('../data/deliver[0])
////////////////////////////////function 4///////////////////////////////////////

function mostEconomicalBowler2015(matchesJson,deliveryJson) {

      let idOf2015 = matchesJson.filter((element)=> element.season == 2015).map((element)=> parseInt(element.id));
       //finds name of all the bowler in year 2015 and adds their balls and rus conceded on that delivery
      let bowler=deliveryJson.reduce((bowlerObj,currentBall)=>{        
            if(idOf2015.includes(parseInt(currentBall.match_id))){
                  //let name=currentBall.bowler;
                  let runs=parseInt(currentBall.total_runs)
                  if(bowlerObj.hasOwnProperty(currentBall.bowler)){
                        if(currentBall.wide_runs==='0'&&currentBall.noball_runs==='0') {
                              bowlerObj[currentBall.bowler][0]+=1;
                        }
                        bowlerObj[currentBall.bowler][1]+=runs;
                  }else{
                        bowlerObj[currentBall.bowler]=[1,runs];
                  }
            }
            return bowlerObj;
      },{})
      //console.log(bowler)
      for(let bowlerName in bowler){                //change the value of each bowler to his economy
            let overs=bowler[bowlerName][0]/6
            let economy=bowler[bowlerName][1]/overs;
            bowler[bowlerName]=economy;
      }
      const sortable = Object.entries(bowler)       //creating an array to sort        
      sortable.sort(function(a, b){
            return a[1] - b[1]; 
      })     
      let mostEconomicalBowlerArray=[];
            
      for(let i=0;i<10;i++){
            mostEconomicalBowlerArray[i]=sortable[i];
      }    
     // console.log(mostEconomicalBowlerArray)        
      return mostEconomicalBowlerArray;
}
let mostEcoResult=mostEconomicalBowler2015(matchesOfIpl,deliveriesOfIpl)
// console.log(mostEcoResult)


  ///////////////////////////////////function 5///////////////////////////////////////////
  //(runs made by virat kohli)

  function kohliRuns2016(matchesJson,deliveryJson) {
      let idOf2016 = matchesJson.filter((match)=> match.season == 2016).map((match)=> parseInt(match.id));
      let kohliRuns=deliveryJson.reduce((totalRuns,currentMatch)=>{
            if(idOf2016.includes(parseInt(currentMatch.match_id))){
                  if(currentMatch.batsman==='V Kohli'){
                        let number=parseInt(currentMatch.batsman_runs);
                        totalRuns[currentMatch.batsman]+=number;
                  }
            }
            return totalRuns;
      },{'V Kohli':0})
        return kohliRuns
  }
  let totalKohliRunsOf2016=kohliRuns2016(matchesOfIpl,deliveriesOfIpl)
console.log(totalKohliRunsOf2016)

  ////////////////////////////////////function 6/////////////////////////////////////////
  // number of batsman got bowled by bumrah 
  function bumrahBowled2016(matchesJson,deliveryJson) {
      let idOf2016 = matchesJson.filter((element)=> element.season == 2016).map((element)=> parseInt(element.id));
      let bowledByBumrah=deliveryJson.reduce((batsmanObject,balls)=>{
            if(idOf2016.includes(parseInt(balls.match_id))){
                  if(balls.bowler==='JJ Bumrah'&&balls.dismissal_kind==='bowled'){
                        batsmanObject[balls.batsman]=0;
                  }
            }
            return batsmanObject;
      },{})
     return  Object.keys(bowledByBumrah).length
  }
  let bowledByBumrah=bumrahBowled2016(matchesOfIpl,deliveriesOfIpl)

  ////////////////////////////////////exports//////////////////////////////////
  module.exports ={
      matchesPerYear,
      matchesWonPerTeam,
      extraRunsConceded2016,
      mostEconomicalBowler2015,
      kohliRuns2016,
      bumrahBowled2016
  }
